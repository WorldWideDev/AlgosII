// Add Song from input/btn

var pList = new MusicPlayer();

var addSongBtn = document.getElementById("addSongBtn");
var nextSongBtn = document.getElementById("nextSongBtn");

addSongBtn.addEventListener("click", function(){
    var songTitleInput = document.getElementById("newSongText");
    pList.AddSong(songTitleInput.value);
    songTitleInput.value = "";
})