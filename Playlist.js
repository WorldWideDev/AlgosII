function SLNode(value){
    this.value = value;
    this.next = null;
}

function SLQueue(){
    this.head = null;
    this.tail = null;
   
}

SLQueue.prototype.Front = function(){
    return this.head.value;
}
SLQueue.prototype.enqueue = function(val){


    // 1) initialize new node object

    const newNode = new SLNode(val);

    // EDGE CASES
        // what if empty??

    if(this.isEmpty()){
        this.head = newNode;
    }

    else {
    // 2) point curr tail.next to new node
        this.tail.next = newNode;
    // 3) point tail to newNode
    }

    this.tail = newNode;
    
    return this;
}

SLQueue.prototype.dequeue = function(){
    // EDGE CASES
    
    // empty list
    if(this.isEmpty()){
        console.log("THIS IS EMPTY YOU SILLY GOOSE");
        return;
    }
   

    // 1) capture value
    var val = this.Front();

    // 2) move head pointer
    this.head = this.head.next;

    // single node Empty now?
    if(this.isEmpty()){
        this.tail = null;
    }
    // 3) return value
    return val;
}

SLQueue.prototype.isEmpty = function(){
    return (this.head === null);
}

SLQueue.prototype.size = function(){



    var count = 0;
    
    var current = this.head;

    while(current != null){
        count++;
        current = current.next;
    }

    return count;
}

function MusicPlayer(){
    this.IsPlaying = false;
    this.PList = new SLQueue();

}

MusicPlayer.prototype.AddSong = function(songName){
    this.PList.enqueue(songName);
}

MusicPlayer.prototype.PlayNext = function() {

    // 1) retrieve previously playing song node's value
    var song = this.PList.dequeue();

    // 2) print to console: Song Name
    console.log(`Now playing: ${song}`);
}
